import { Controller, Get } from '@nestjs/common';
import { StatisticsService } from './statistics.service';
import { ProjectCostPerformance } from './entities/project-cost-performance.entity';
import { ProjectPerformance } from './entities/project-performance.entity';
import { ProjectPhaseResourceTimeEntity } from './entities/project-phase-resource-time.entity';
import { ProjectPhaseCompletedEntity } from './entities/project-phase-completed.entity';
import { TimeVsResourceCapacityByDateEntity } from './entities/time-vs-resource-capacity-by-date.entity';

@Controller('statistics')
export class StatisticsController {
  constructor(
    private readonly statisticsService: StatisticsService,
    ) {}

  @Get()
  getStatistics(): string {
    return this.statisticsService.getStatistics();
  }

  @Get('/project-cost-performance')
  async getProjectCostPerformance(): Promise<ProjectCostPerformance | undefined> {
    return this.statisticsService.getFirstProjectCostPerformance();
  }

  @Get('/project-performance')
  async getProjectPerformance(): Promise<ProjectPerformance> {
    return this.statisticsService.getProjectPerformance();
  }

  @Get('project-phase-resource-time')
  async getProjectPhaseResourceTime(): Promise<ProjectPhaseResourceTimeEntity[]> {
    return this.statisticsService.getProjectPhaseResourceTime();
  }

  @Get('project-phase-completed')
  async getProjectPhaseCompleted(): Promise<ProjectPhaseCompletedEntity[]> {
    return this.statisticsService.getProjectPhaseCompleted();
  }

  @Get('time-vs-resource-capacity-by-date') // Adjusted path here
  async getTimeVsResourceCapacityByDate(): Promise<TimeVsResourceCapacityByDateEntity[]> {
    return this.statisticsService.getTimeVsResourceCapacityByDate();
  }
}
