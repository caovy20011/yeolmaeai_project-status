import { Module } from '@nestjs/common';
import { StatisticsController } from './statistics.controller';
import { StatisticsService } from './statistics.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectCostPerformance } from './entities/project-cost-performance.entity';
import { ProjectPerformance } from './entities/project-performance.entity';
import { ProjectPhaseResourceTimeEntity } from './entities/project-phase-resource-time.entity';
import { ProjectPhaseCompletedEntity } from './entities/project-phase-completed.entity';
import { TimeVsResourceCapacityByDateEntity } from './entities/time-vs-resource-capacity-by-date.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProjectCostPerformance,
      ProjectPerformance,
      ProjectPhaseResourceTimeEntity,
      ProjectPhaseCompletedEntity,
      TimeVsResourceCapacityByDateEntity
    ]),
  ],
  controllers: [StatisticsController],
  providers: [StatisticsService],
})
export class StatisticsModule {}
