// src/modules/statistics/entities/time-vs-resource-capacity-by-date.entity.ts

import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { ProjectPhaseEntity } from './project-phase.entity';

@Entity({ name: 'time-vs-resource-capacity-by-date' })
export class TimeVsResourceCapacityByDateEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'date' })
  date: Date;

  @Column({ type: 'int', default: 0 })
  hours: number;

  @Column({ type: 'int', default: 0 })
  resourceCapacity: number;

  @Column({ name: 'projectPhaseId' })
  projectPhaseId: number;

  @ManyToOne(() => ProjectPhaseEntity, projectPhase => projectPhase.timeVsResourceCapacity)
  @JoinColumn({ name: 'projectPhaseId' })
  projectPhase: ProjectPhaseEntity;
}
