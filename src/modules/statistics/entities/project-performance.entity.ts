// src/modules/statistics/entities/project-performance.entity.ts

import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'project-performance' })
export class ProjectPerformance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0 })
  spi: number;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0 })
  cpi: number;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0 })
  pv: number;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0 })
  ev: number;
}
