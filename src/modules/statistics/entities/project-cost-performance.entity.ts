// src/modules/statistics/entities/project-cost-performance.entity.ts

import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'project-cost-performance' })
export class ProjectCostPerformance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int', default: 0 })
  totalBudget: number;

  @Column({ type: 'int', default: 0 })
  actualCost: number;
}
