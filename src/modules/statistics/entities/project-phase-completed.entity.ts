// src/modules/statistics/entities/project-phase-completed.entity.ts

import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { ProjectPhaseEntity } from './project-phase.entity';

@Entity({ name: 'project-phase-completed' })
export class ProjectPhaseCompletedEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'decimal', precision: 10, scale: 2, default: 0 })
  percentage: number;

  @Column({ name: 'projectPhaseId' })
  projectPhaseId: number;

  @ManyToOne(() => ProjectPhaseEntity, projectPhase => projectPhase.projectPhaseCompleted)
  @JoinColumn({ name: 'projectPhaseId' })
  projectPhase: ProjectPhaseEntity;
}
