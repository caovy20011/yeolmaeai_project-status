// src/modules/statistics/entities/project-phase-resource-time.entity.ts

import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { ResourceEntity } from './resource.entity';
import { ProjectPhaseEntity } from './project-phase.entity';

@Entity({ name: 'project-phase-resource-time' })
export class ProjectPhaseResourceTimeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int', default: 0 })
  hours: number;

  @Column({ type: 'int' })
  resourceId: number;

  @Column({ type: 'int' })
  projectPhaseId: number;

  @ManyToOne(() => ResourceEntity, resource => resource.projectPhaseResourceTimes)
  @JoinColumn({ name: 'resourceId' })
  resource: ResourceEntity;

  @ManyToOne(() => ProjectPhaseEntity, projectPhase => projectPhase.projectPhaseResourceTimes)
  @JoinColumn({ name: 'projectPhaseId' })
  projectPhase: ProjectPhaseEntity;
}
