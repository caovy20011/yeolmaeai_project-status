// src/modules/statistics/entities/project-phase.entity.ts

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ProjectPhaseResourceTimeEntity } from './project-phase-resource-time.entity';
import { ProjectPhaseCompletedEntity } from './project-phase-completed.entity';
import { TimeVsResourceCapacityByDateEntity } from './time-vs-resource-capacity-by-date.entity';

@Entity({ name: 'project-phase' })
export class ProjectPhaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100, nullable: false })
  name: string;

  @OneToMany(() => ProjectPhaseResourceTimeEntity, projectPhaseResourceTime => projectPhaseResourceTime.projectPhase)
  projectPhaseResourceTimes: ProjectPhaseResourceTimeEntity[];

  @OneToMany(() => ProjectPhaseCompletedEntity, projectPhaseCompleted => projectPhaseCompleted.projectPhase)
  projectPhaseCompleted: ProjectPhaseCompletedEntity[];

  @OneToMany(() => TimeVsResourceCapacityByDateEntity, timeVsResourceCapacity => timeVsResourceCapacity.projectPhase)
  timeVsResourceCapacity: TimeVsResourceCapacityByDateEntity[];
}
