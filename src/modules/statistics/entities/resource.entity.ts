// src/modules/statistics/entities/resource.entity.ts

import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ProjectPhaseResourceTimeEntity } from './project-phase-resource-time.entity';

@Entity('resource')
export class ResourceEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50, nullable: false })
  name: string;

  @OneToMany(() => ProjectPhaseResourceTimeEntity, projectPhaseResourceTime => projectPhaseResourceTime.resource)
  projectPhaseResourceTimes: ProjectPhaseResourceTimeEntity[];
}
