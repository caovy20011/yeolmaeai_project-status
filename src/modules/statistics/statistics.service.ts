import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProjectCostPerformance } from './entities/project-cost-performance.entity';
import { Repository } from 'typeorm';
import { ProjectPerformance } from './entities/project-performance.entity';
import { ProjectPhaseResourceTimeEntity } from './entities/project-phase-resource-time.entity';
import { ProjectPhaseCompletedEntity } from './entities/project-phase-completed.entity';
import { TimeVsResourceCapacityByDateEntity } from './entities/time-vs-resource-capacity-by-date.entity';

@Injectable()
export class StatisticsService {
  constructor(
    @InjectRepository(ProjectCostPerformance)
    private readonly projectCostPerformanceRepository: Repository<ProjectCostPerformance>,
    @InjectRepository(ProjectPerformance)
    private readonly projectPerformanceRepository: Repository<ProjectPerformance>,
    @InjectRepository(ProjectPhaseResourceTimeEntity)
    private readonly projectPhaseResourceTimeRepository: Repository<ProjectPhaseResourceTimeEntity>,
    @InjectRepository(ProjectPhaseCompletedEntity)
    private readonly projectPhaseCompletedRepository: Repository<ProjectPhaseCompletedEntity>,
    @InjectRepository(TimeVsResourceCapacityByDateEntity)
    private readonly timeVsResourceCapacityRepository: Repository<TimeVsResourceCapacityByDateEntity>,
  ) {}

  getStatistics(): string {
    return 'This is the statistics service!';
  }

  async getFirstProjectCostPerformance(): Promise<ProjectCostPerformance | undefined> {
    const projectCostPerformances = await this.projectCostPerformanceRepository.find({
      take: 1,
      order: { id: 'ASC' }, // Assuming 'id' is the primary key, change it if necessary
    });

    return projectCostPerformances[0]; // Return the first (and only) record
  }

  async getProjectPerformance(): Promise<ProjectPerformance> {
    const projectPerformances = await this.projectPerformanceRepository.find({
      take: 1,
      order: { id: 'ASC' }, // Assuming 'id' is the primary key, change it if necessary
    });

    return projectPerformances[0]; // Return the first (and only) record
  }

  async getProjectPhaseResourceTime(): Promise<ProjectPhaseResourceTimeEntity[]> {
    return this.projectPhaseResourceTimeRepository.find();
  }

  async getProjectPhaseCompleted(): Promise<ProjectPhaseCompletedEntity[]> {
    return await this.projectPhaseCompletedRepository.find();
  }

  async getTimeVsResourceCapacityByDate(): Promise<TimeVsResourceCapacityByDateEntity[]> {
    return this.timeVsResourceCapacityRepository.find();
  }
}
