USE `project-status`;

-- Insert records into the 'time-vs-resource-capacity-by-date' table
INSERT INTO `time-vs-resource-capacity-by-date` (date, projectPhaseId, hours, resourceCapacity)
VALUES
    ('2023-10-03', (SELECT id FROM `project-phase` WHERE name = 'Project Planning Stage'), 88, 56),
    ('2023-10-04', (SELECT id FROM `project-phase` WHERE name = 'Project Planning Stage'), 16, 32),
    ('2023-10-05', (SELECT id FROM `project-phase` WHERE name = 'Project Planning Stage'), 24, 32),
    ('2023-10-05', (SELECT id FROM `project-phase` WHERE name = 'Project Planning Stage'), 16, 16),
    ('2023-10-03', (SELECT id FROM `project-phase` WHERE name = 'Monitor & Control'), 20, 10),
    ('2023-10-10', (SELECT id FROM `project-phase` WHERE name = 'Monitor & Control'), 16, 16),
    ('2023-10-01', (SELECT id FROM `project-phase` WHERE name = 'Project Initiation Stage'), 16, 16),
    ('2023-10-02', (SELECT id FROM `project-phase` WHERE name = 'Project Initiation Stage'), 24, 36),
    ('2023-10-10', (SELECT id FROM `project-phase` WHERE name = 'Execution Stage'), 40, 20),
    ('2023-10-11', (SELECT id FROM `project-phase` WHERE name = 'Execution Stage'), 8, 8),
    ('2023-10-15', (SELECT id FROM `project-phase` WHERE name = 'Execution Stage'), 16, 8),
    ('2023-10-16', (SELECT id FROM `project-phase` WHERE name = 'Execution Stage'), 100, 8);
