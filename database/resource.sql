-- Use the project-status database
USE `project-status`;

-- Insert data into the resource table
INSERT INTO `resource` (`name`) VALUES
  ('Resource A'),
  ('Resource B'),
  ('Resource C');
