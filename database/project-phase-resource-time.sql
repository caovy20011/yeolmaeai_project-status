USE `project-status`;

-- Insert data into the project-phase-resource-time table
INSERT INTO `project-phase-resource-time` (id, hours, projectPhaseId, resourceId) VALUES
    (DEFAULT, 80, (SELECT id FROM `project-phase` WHERE name = 'Project Planning Stage'), (SELECT id FROM `resource` WHERE name = 'Resource A')),
    (DEFAULT, 56, (SELECT id FROM `project-phase` WHERE name = 'Project Planning Stage'), (SELECT id FROM `resource` WHERE name = 'Resource B')),
    (DEFAULT, 8, (SELECT id FROM `project-phase` WHERE name = 'Project Planning Stage'), (SELECT id FROM `resource` WHERE name = 'Resource C')),
    (DEFAULT, 24, (SELECT id FROM `project-phase` WHERE name = 'Project Initiation Stage'), (SELECT id FROM `resource` WHERE name = 'Resource A')),
    (DEFAULT, 8, (SELECT id FROM `project-phase` WHERE name = 'Project Initiation Stage'), (SELECT id FROM `resource` WHERE name = 'Resource B')),
    (DEFAULT, 8, (SELECT id FROM `project-phase` WHERE name = 'Project Initiation Stage'), (SELECT id FROM `resource` WHERE name = 'Resource C')),
    (DEFAULT, 36, (SELECT id FROM `project-phase` WHERE name = 'Monitor & Control'), (SELECT id FROM `resource` WHERE name = 'Resource A')),
    (DEFAULT, 164, (SELECT id FROM `project-phase` WHERE name = 'Execution Stage'), (SELECT id FROM `resource` WHERE name = 'Resource B')),
    (DEFAULT, 16, (SELECT id FROM `project-phase` WHERE name = 'Closure Stage'), (SELECT id FROM `resource` WHERE name = 'Resource B')),
    (DEFAULT, 40, (SELECT id FROM `project-phase` WHERE name = 'Closure Stage'), (SELECT id FROM `resource` WHERE name = 'Resource C'));
