USE `project-status`;

-- Record 1
INSERT INTO `project-phase-completed` (percentage, projectPhaseId)
SELECT 60, id
FROM `project-phase`
WHERE name = 'Project Planning Stage';

-- Record 2
INSERT INTO `project-phase-completed` (percentage, projectPhaseId)
SELECT 13.3, id
FROM `project-phase`
WHERE name = 'Monitor & Control';

-- Record 3
INSERT INTO `project-phase-completed` (percentage, projectPhaseId)
SELECT 100, id
FROM `project-phase`
WHERE name = 'Project Initiation Stage';

-- Record 4
INSERT INTO `project-phase-completed` (percentage, projectPhaseId)
SELECT 0, id
FROM `project-phase`
WHERE name = 'Execution Stage';

-- Record 5
INSERT INTO `project-phase-completed` (percentage, projectPhaseId)
SELECT 0, id
FROM `project-phase`
WHERE name = 'Closure Stage';
