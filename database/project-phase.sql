-- Change to the 'project-status' database
USE `project-status`;

-- Insert data into the 'project-phase' table
INSERT INTO `project-phase` (name) VALUES
    ('Project Planning Stage'),
    ('Monitor & Control'),
    ('Project Initiation Stage'),
    ('Execution Stage'),
    ('Closure Stage');
