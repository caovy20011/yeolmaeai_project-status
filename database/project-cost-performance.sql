-- Ensure that you are using the correct database
USE `project-status`;

-- Insert data into the 'project-cost-performance' table
INSERT INTO `project-cost-performance` (totalBudget, actualCost)
VALUES (93332, 48048);
